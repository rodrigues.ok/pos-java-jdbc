package pos_java_jdbc.pos_java_jdbc;

import java.util.List;

import org.junit.Test;

import conexaojdbc.SingleConnection;
import dao.UserPosDAO;
import model.BeanUserFone;
import model.Telefone;
import model.Userposjava;

public class TesteBancoJdbc {

	@Test
	public void initBanco() {

		UserPosDAO userPosDAO = new UserPosDAO();
		Userposjava userposjava = new Userposjava();

		userposjava.setNome("Max");
		userposjava.setEmail("max@gmail.com");

		userPosDAO.Salvar(userposjava);

	}

	@Test
	public void initListar() {

		UserPosDAO dao = new UserPosDAO();
		try {
			List<Userposjava> list = dao.listar();

			for (Userposjava userposjava : list) {
				System.out.println(userposjava);
				System.out.println("-----------------------------------------------------------------");

			}
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Test
	public void initBuscar() {

		UserPosDAO dao = new UserPosDAO();
		try {
			Userposjava userposjava = dao.buscar(4L);

			System.out.println(userposjava);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Test
	public void initAtualizar() {

		try {

			UserPosDAO dao = new UserPosDAO();

			Userposjava objetoBanco;

			objetoBanco = dao.buscar(5L);

			objetoBanco.setNome("Nome Atualizado");

			dao.atualizar(objetoBanco);

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	@Test
	public void initDeletar() {
		try {

			UserPosDAO dao = new UserPosDAO();
			dao.deletar(6L);

		} catch (Exception e) {

		}

	}

	@Test
	public void testeInsertTelefone() {

		Telefone telefone = new Telefone();
		telefone.setNumero("(61) 9890-2389");
		telefone.setTipo("celular");
		telefone.setUsuario(12L);

		UserPosDAO dao = new UserPosDAO();

		dao.salvarTelefone(telefone);

	}

	@Test
	public void TesteCarregaFoneUser() {

		UserPosDAO dao = new UserPosDAO();

		List<BeanUserFone> beanUserFones = dao.listaUserFone(12L);
		for (BeanUserFone beanUserFone : beanUserFones) {
			System.out.println(beanUserFone);
			System.out.println("---------------------------------------");
		}

	}
	

}
